﻿namespace Sample_RPG_Framework
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.createcharacterlabel = new System.Windows.Forms.Label();
            this.PerceptiontextBox = new System.Windows.Forms.TextBox();
            this.StaminatextBox = new System.Windows.Forms.TextBox();
            this.EndurancetextBox = new System.Windows.Forms.TextBox();
            this.AgilitytextBox = new System.Windows.Forms.TextBox();
            this.StrengthtextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.NametextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.createcharacterlabel);
            this.groupBox1.Controls.Add(this.PerceptiontextBox);
            this.groupBox1.Controls.Add(this.StaminatextBox);
            this.groupBox1.Controls.Add(this.EndurancetextBox);
            this.groupBox1.Controls.Add(this.AgilitytextBox);
            this.groupBox1.Controls.Add(this.StrengthtextBox);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.NametextBox);
            this.groupBox1.Location = new System.Drawing.Point(221, 349);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // createcharacterlabel
            // 
            this.createcharacterlabel.AutoSize = true;
            this.createcharacterlabel.Location = new System.Drawing.Point(25, 18);
            this.createcharacterlabel.Name = "createcharacterlabel";
            this.createcharacterlabel.Size = new System.Drawing.Size(87, 13);
            this.createcharacterlabel.TabIndex = 7;
            this.createcharacterlabel.Text = "Create Character";
            // 
            // PerceptiontextBox
            // 
            this.PerceptiontextBox.Location = new System.Drawing.Point(112, 65);
            this.PerceptiontextBox.Name = "PerceptiontextBox";
            this.PerceptiontextBox.Size = new System.Drawing.Size(100, 20);
            this.PerceptiontextBox.TabIndex = 6;
            this.PerceptiontextBox.Text = "Perception";
            // 
            // StaminatextBox
            // 
            this.StaminatextBox.Location = new System.Drawing.Point(6, 65);
            this.StaminatextBox.Name = "StaminatextBox";
            this.StaminatextBox.Size = new System.Drawing.Size(100, 20);
            this.StaminatextBox.TabIndex = 5;
            this.StaminatextBox.Text = "Stamina";
            // 
            // EndurancetextBox
            // 
            this.EndurancetextBox.Location = new System.Drawing.Point(218, 39);
            this.EndurancetextBox.Name = "EndurancetextBox";
            this.EndurancetextBox.Size = new System.Drawing.Size(100, 20);
            this.EndurancetextBox.TabIndex = 4;
            this.EndurancetextBox.Text = "Endurance";
            // 
            // AgilitytextBox
            // 
            this.AgilitytextBox.Location = new System.Drawing.Point(112, 39);
            this.AgilitytextBox.Name = "AgilitytextBox";
            this.AgilitytextBox.Size = new System.Drawing.Size(100, 20);
            this.AgilitytextBox.TabIndex = 3;
            this.AgilitytextBox.Text = "Agility";
            // 
            // StrengthtextBox
            // 
            this.StrengthtextBox.Location = new System.Drawing.Point(6, 39);
            this.StrengthtextBox.Name = "StrengthtextBox";
            this.StrengthtextBox.Size = new System.Drawing.Size(100, 20);
            this.StrengthtextBox.TabIndex = 2;
            this.StrengthtextBox.Text = "Strength";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(237, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // NametextBox
            // 
            this.NametextBox.Location = new System.Drawing.Point(131, 13);
            this.NametextBox.Name = "NametextBox";
            this.NametextBox.Size = new System.Drawing.Size(100, 20);
            this.NametextBox.TabIndex = 0;
            this.NametextBox.Text = "Name";
            this.NametextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 461);
            this.Controls.Add(this.groupBox1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox NametextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox AgilitytextBox;
        private System.Windows.Forms.TextBox StrengthtextBox;
        private System.Windows.Forms.TextBox EndurancetextBox;
        private System.Windows.Forms.TextBox StaminatextBox;
        private System.Windows.Forms.Label createcharacterlabel;
        private System.Windows.Forms.TextBox PerceptiontextBox;
    }
}


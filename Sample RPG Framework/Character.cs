﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_RPG_Framework
{
    class Player
    {
        public int x { get; set; }
        public int y { get; set; }
        public string name { get; set; }
        public string warcry { get; set; }

        public int strength { get; set; }
        public int agility { get; set; }
        public int endurance { get; set; }
        public int stamina { get; set; }
        public int perception { get; set; }
        
    }
}
